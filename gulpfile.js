var gulp         = require('gulp'),
    styleInject  = require("gulp-style-inject"),
    inlineCss    = require('gulp-inline-css'),
    htmlbeautify = require('gulp-html-beautify'),
    sass         = require('gulp-sass');
    handlebars   = require('gulp-compile-handlebars'),
    rename       = require("gulp-rename"),
    replace      = require('gulp-replace');

// ---
var presentationMode = true;
// ---

gulp.task('css', function(){
  return gulp.src('./src/styles.scss')
    .pipe(sass())
    .pipe(gulp.dest('./src'));
});

gulp.task('dist', function(){
  return gulp.src('./src/*.html')
    .pipe(replace('{@', '{{'))
    .pipe(replace('@}', '}}'))
    .pipe(styleInject())
    .pipe(inlineCss({
      preserveMediaQueries: true
    }))
    .pipe(htmlbeautify({
      indent_size: 2
    }))
    .pipe(gulp.dest("./dist"));
});

// partials start
var partialsData;
var designPartials = {
    shippingAddress: 'Darrell Wood<br>98 Parker Ranch Dr<br>Henderson, Nevada, 89012-2614<br>United States<br>T: 702 373 5210',
    billingAddress: 'Darrell Wood<br>98 Parker Ranch Dr<br>Henderson, Nevada, 89012-2614<br>United States<br>T: 702 373 5210',
    shippingAddress2: 'Darrell Wood<br>98 Parker Ranch Dr<br>Henderson, Nevada, 89012-2614<br>United States<br>T: 702 373 5210',
    billingAddress2: 'Darrell Wood<br>98 Parker Ranch Dr<br>Henderson, Nevada, 89012-2614<br>United States<br>T: 702 373 5210',

    sellerInfo: 'Darrell Wood<br>98 Parker Ranch Dr<br>Henderson, Nevada, 89012-2614<br>United States<br>T: 702 373 5210',
    purchaserInfo: 'Bullion Exchanges<br>32 West 47th Street<br>Booth 41-46<br>New York, NY 10036<br>United State<br>T: 212-354-1517',
    shippingInfo: 'Customer Service<br>32 West 47th Street<br>Booth 41-46<br>New York, NY 10036',
    fo: 'You must provide Bullion Exchanges with the tracking information for your package which must be postdated within 2 business days of the sale confirmation. <a style="text-decoration:none" href="mailto:customerservice@bullionexchanges.com">customerservice@bullionexchanges.com</a>',
    purchaseOrderNumber: 'PO-100187401',
    purchaseOrderDate: 'MAY 28, 2017 10:26:14 AM EDT',
    po_grandTotal: "$403.73",
    po_paymentMethod: "Wire Transfer",

    paypal_url: "http://example.com/paypalrequestpayment/index/paypaypalrequest/order_id/100187406",
    paypal_url_cancel: "#",
    creditMemo: "100000448",
    productName: "10 gram Gold Bar PAMP Suisse Lady Fortuna Veriscan .9999 Fine (In Assay)",
    productUrl: "#",
    alertPrice: "$425.00",
    currentPrice: "$424.50",
    metalName: "Gold",
    operatorPrice: "Raised Above",
    pricealertUrl: '#',

    alertProductImgUrl: "http://i.imgur.com/6JqwRK9.jpg",
    alertProductUrl: '#',
    alertProductName: '1 oz American Flag Silver Bar .999 Silver (Sealed)',
    alertProductUnsubscribeUrl: '#',

    checkAddress: 'Pay to the Order Of: Bullion Exchange, LLC.<br>Address:<br>32 West 47th Street, Booth 41-46<br>New York, NY 10036',
    invoiceAddress: 'Bank: TD Bank, NA.<br>Company: Bullion Exchange, LLC<br>Company Address: 32 W 47th Street Booth 41-46, New York, NY 10036.<br>Routing #: 026013673.<br>Checking Account #: 4321013594',
    time: 'Monday - Friday 9am - 5pm EST',
    time2: 'MON - FRI  |  9 AM - 5 PM EST',
    phone: '800.852.6884',
    phone2: '800.852.6884',
    username: 'John Doe',
    username2: 'John Doe',
    username3: 'John Doe',
    username4: 'John Doe',
    username5: 'John Doe',
    username6: 'John Doe',
    mailAddress: 'john_doe@email.com',
    mailAddress2: 'john_doe@email.com',
    ordernumber: '100124970',
    ordernumber2: '100187337',
    shipping_date: '06/01/2017',
    orderdate: 'MAY 28, 2017 10:26:14 AM EDT',
    shippingMethod: 'Flat Rate',
    shippingMethod2: 'Flat Rate',
    paymentMethod: 'PayPal',
    paymentTitle: 'Credit Card',
    points: '100',
    days: '7',
    checkTotal: '$578.40',
    invoiceNumber: '9400116901511203866548',
    newAccountEmail: 'john_doe@email.com',
    newAccountPassword_visible: '12345abcd',
    newAccountPassword: 'The password you have chosen when creating this account.',
    trackNumber1: 'US203866548Z',
    trackNumber2: 'US203866548Z',
    trackNumber3: 'DH657203866548',
    carrier1: 'USPS',
    carrier2: 'USPS',
    carrier3: 'DHL',
    supportMail: "customerservice@bullionexchanges.com",
    shippedDate: 'MAY 28, 2017 10:26:14 AM EDT',
    shippedBy: 'USPS First Class Mail',
    abandonedCartLink: "#",
    confirmAccountLink: '#',
    resetPasswordLink: '#',
    facebookLink: '#',
    twitterLink: '#',
    pinterestLink: '#',
    linkedinLink: '#',
    instagramLink: '#',
    googleLink: '#',
    shipmentComment: 'Shipment comment blah blah',
    orderComment: 'Order comment blah blah',
    orderComment2: 'Order comment blah blah',
    mistakeEmailLink: '#'
};

var cmsPartials = {
    shippingAddress: "{@var order.getShippingAddress().format('html')@}",
    billingAddress: "{@var order.getBillingAddress().format('html')@}",
    shippingAddress2: "{@var order.shipping_address.format('html')@}",
    billingAddress2: "{@var order.billing_address.format('html')@}",

    sellerInfo: "{@var customer.getDefaultBillingAddress().format('html')@}",
    purchaserInfo: 'Bullion Exchanges<br>32 West 47th Street<br>Booth 41-46<br>New York, NY 10036<br>United State<br>T: 212-354-1517',
    shippingInfo: 'Customer Service<br>32 West 47th Street<br>Booth 41-46<br>New York, NY 10036',
    purchaseOrderNumber: '{@var order.increment_id@}',
    purchaseOrderDate: "{@var order.getCreatedAtFormated('long')@}",
    po_grandTotal: "{@var grand_total@}",
    po_paymentMethod: "{@var disbursement_method@}",

    paypal_url: "{@var url@}",
    paypal_url_cancel: "{@var url_cancel@}",
    creditMemo: "{@var creditmemo.increment_id@}",
    productName: "{@htmlescape var=$product.name@}",
    productUrl: "{@htmlescape var=$product.getProductUrl()@}",
    alertPrice: "{@var alert_price@}",
    currentPrice: "{@var current_price@}",
    metalName: "{@var metal_name@}",
    operatorPrice: "{@var operator_price@}",
    pricealertUrl: "{@store url='pricealert/market/'@}",

    alertProductImgUrl: "<?php echo $this->helper('catalog/image')->init($product, 'thumbnail')->resize(125, 125) ?>",
    alertProductUrl: '<?php echo $product->getProductUrl() ?>',
    alertProductName: '<?php echo $this->escapeHtml($product->getName()) ?>',
    alertProductUnsubscribeUrl: '<?php echo $this->getProductUnsubscribeUrl($product->getId()) ?>',

    checkAddress: 'Pay to the Order Of: Bullion Exchange, LLC.<br>Address:<br>32 West 47th Street, Booth 41-46<br>New York, NY 10036',
    invoiceAddress: 'Bank: TD Bank, NA.<br>Company: Bullion Exchange, LLC<br>Company Address: 32 W 47th Street Booth 41-46, New York, NY 10036.<br>Routing #: 026013673.<br>Checking Account #: 4321013594',
    time: 'Monday - Friday 9am - 5pm EST',
    time2: 'MON - FRI  |  9 AM - 5 PM EST',
    phone: "800.852.6884",
    phone2: '800.852.6884',
    username: '{@htmlescape var=$order.getCustomerName()@}',
    username2: '{@htmlescape var=$customer.name@}',
    username3: '{@var customer.getFirstname()@} {@var customer.getLastname()@}',
    username4: '{@htmlescape var=$customer.getName()@}',
    username5: '{@var name@}',
    username6: '{@var customerName@}',
    mailAddress: '{@htmlescape var=$customer.getEmail()@}',
    mailAddress2: '{@htmlescape var=$order.getCustomerEmail()@}',
    ordernumber: '{@var order.increment_id@}',
    ordernumber2: '{@var order_id@}',
    shipping_date: '{@var shipping_date@}',
    orderdate: "{@var order.getCreatedAtFormated('long')@}",
    shippingMethod: '{@var order.getShippingDescription()@}',
    shippingMethod2: '{@var order.shipping_description@}',
    paymentMethod: '{@var payment_html@}',
    paymentTitle: '{@var order.getPayment().getMethodInstance().getTitle()@}',
    points: '{@var points@}',
    days: '{@var days@}',
    checkTotal: '{@var ORDER_TOTAL@}',
    invoiceNumber: '{@var invoice.increment_id@}',
    newAccountEmail: '{@var customer.email@}',
    newAccountPassword_visible: '{@htmlescape var=$customer.password@}',
    newAccountPassword: 'The password you have chosen when creating this account.',
    trackNumber1: 'US203866548Z',
    trackNumber2: 'US203866548Z',
    trackNumber3: 'DH657203866548',
    carrier1: 'USPS',
    carrier2: 'USPS',
    carrier3: 'DHL',
    supportMail: "{@config path='trans_email/ident_support/email'@}",
    shippedDate: '{@var shipment.getCreatedAtStoreDate()@}',
    shippedBy: '{@var order.shipping_description@}',
    abandonedCartLink: "{@var urlmanager.mageUrl('amacartfront/main/order')@}",
    confirmAccountLink: "{@store url='customer/account/confirm/' _query_id='$customer.id' _query_key='$customer.confirmation' _query_back_url='$back_url'@}",
    resetPasswordLink: "{@store url='customer/account/resetpassword/' _query_id='$customer.id' _query_token='$customer.rp_token'@}",
    facebookLink: 'https://www.facebook.com/bullionexchanges',
    twitterLink: 'https://twitter.com/goldexchangenyc',
    pinterestLink: 'https://www.pinterest.com/bullionexchange/',
    linkedinLink: 'https://www.linkedin.com/company/bullion-exchanges-llc?trk=biz-companies-cym',
    instagramLink: 'https://www.instagram.com/bullionexchanges/',
    googleLink: 'https://plus.google.com/+BullionexchangesNYC',
    shipmentComment: '{@var comment@}',
    orderComment: '{@var comment@}',
    orderComment2: '{@var order.getEmailCustomerNote()@}',
    mistakeEmailLink: '#'
};
// aprtials end


if (presentationMode) {
    partialsData = designPartials;
    batchData = ['./src/templates/partials'];
} else {
    partialsData = cmsPartials;
    batchData = ['./src/templates/cms-partials'];
}

gulp.task('templates', function () {
    var templateData = { },
    options = {
        ignorePartials: true,
        partials : partialsData,
        batch : batchData
        // helpers : {
        //     capitals : function(str){
        //         return str.toUpperCase();
        //     }
        // }
    };

    return gulp.src('./src/templates/*.hbs')
        .pipe(handlebars(templateData, options))
        .pipe(rename(function (filename) {
          filename.extname = ".html"
        }))
        .pipe(gulp.dest('./src'));
});


gulp.task('watch', function(){
  gulp.watch(['src/styles.scss'], gulp.series('css'));
  gulp.watch(['src/templates/**/*.hbs'], gulp.series('templates'));
});

gulp.task('default', gulp.series('watch'));